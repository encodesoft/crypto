﻿using Coinbase;
using crypto.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace crypto.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void GetPrice()
        {
        
            
        }
        public ActionResult Index()
        {
            return View();
        }
        
        public async Task<ActionResult> ProcessData()
        {
              await CallApi();
            return View("index");
        }

        public void UpdateComission(string description, string currencyName)
        {
            int CoinbaseFee = Convert.ToInt32(ConfigurationManager.AppSettings["CoinbaseFee"].ToString());

            ComissionFee fee = new ComissionFee();
            fee.Fee = CoinbaseFee;
            var lastFee = db.ComissionFees.OrderByDescending(t => t.Id).FirstOrDefault();
            if(lastFee!=null)
            {
                fee.RunningTotal = fee.RunningTotal + CoinbaseFee;
            }
            else
            {
                fee.RunningTotal = CoinbaseFee;
            }
            fee.Descripton = currencyName+":"+description;
            db.ComissionFees.Add(fee);
            db.SaveChanges();

        }
        public void UpdateBalance(decimal amount,string description,string currencyName)
        {
            var lastBalance = db.BankBalances.OrderByDescending(t => t.Id).FirstOrDefault();

            if(lastBalance!=null)
            {
                
                BankBalance balance = new BankBalance();
                balance.Balance = lastBalance.Balance+amount;
                balance.Descripton = currencyName+":"+description;
                db.BankBalances.Add(balance);
                db.SaveChanges();
            }
            else

            {
                BankBalance balance = new BankBalance();
                balance.Balance = amount;
                balance.Descripton = description;
                db.BankBalances.Add(balance);
                db.SaveChanges();
            }
           
        }
       private async Task CallApi()
        {
            int NumberOfPricesContributingToTrend = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfPricesContributingToTrend"].ToString());
            string sec = ConfigurationManager.AppSettings["sec1"];
            string key = ConfigurationManager.AppSettings["key1"];
            int ApiCallEveryNMinutesMin = Convert.ToInt32(ConfigurationManager.AppSettings["ApiCallEveryNMinutesMin"].ToString());
            int ApiCallEveryNMinutesMax = Convert.ToInt32(ConfigurationManager.AppSettings["ApiCallEveryNMinutesMax"].ToString());
            int CoinbaseFee = Convert.ToInt32(ConfigurationManager.AppSettings["CoinbaseFee"].ToString());
            float PercentIncrementForTrend =(float) Convert.ToDecimal(ConfigurationManager.AppSettings["PercentIncrementForTrend"].ToString());

            var currencies = db.Currencies.ToList();
            foreach(var currency in currencies)
            {
                int CurrencyId = currency.Id;

                string CurrencyName = currency.CurrencyName;
                int InitialMoney = currency.InitialMoney;
                int SellAtProfit = currency.SellAtProfit;
                int SellAtLoss = currency.SellAtLoss;
               
                Random rnd = new Random();
                int ApiCallEveryNMinutes = rnd.Next(ApiCallEveryNMinutesMin, ApiCallEveryNMinutesMax);  // 

                var currentPrices = db.Prices.Where(t=>t.CurrencyId== CurrencyId).OrderByDescending(t => t.Id).ToList();
                var previosPrice = currentPrices.FirstOrDefault();
                if (previosPrice != null)
                {
                    var timediff = DateTime.UtcNow - previosPrice.DateCreated;
                    if (timediff.HasValue)
                    {
                        if (timediff.Value.TotalMinutes < ApiCallEveryNMinutes)
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                using (var client = new CoinbaseClient(new ApiKeyConfig { ApiKey = key, ApiSecret = sec }))
                {
                    var spot = await client.Data.GetSpotPriceAsync(CurrencyName);
                    Price price = new Price();
                    if (currentPrices.Count() == 0)
                    {
                        //UpdateBalance(InitialMoney, "Seed Money",CurrencyName);
                        //UpdateBalance((0-InitialMoney), "Fund crypto account", CurrencyName);
                        price.Action = "Initial Funding";
                        price.CurrencySellValue = 0;
                        price.PortfolioValue = InitialMoney;
                        price.BuyValue = 0;

                        price.CurrencyTickerValue = (float)spot.Data.Amount;
                        price.CurrencyId = CurrencyId;
                        price.CurrencyName = CurrencyName;
                        price.Logs = "Initial Funding";
                        price.NumberOfTokensInTheSystem = 0;
                        price.Trend = Trend.NoChange;

                    }
                    else
                    {

                        price.Action = "WAIT";
                        price.CurrencySellValue = 0;
                        price.NumberOfTokensInTheSystem = previosPrice.NumberOfTokensInTheSystem;
                        price.CurrencyTickerValue = (float)spot.Data.Amount;

                        if (price.NumberOfTokensInTheSystem != 0)
                        {
                            price.PortfolioValue = (float)price.NumberOfTokensInTheSystem.Value * (float)price.CurrencyTickerValue;
                            price.BuyValue = previosPrice.BuyValue;
                        }
                        else
                        {
                            price.PortfolioValue = previosPrice.PortfolioValue;
                            price.BuyValue =0;
                        }
                        price.CurrencyId = CurrencyId;
                        price.CurrencyName = CurrencyName;
                        price.Logs = "WAIT";
                        price.Trend = Trend.NoChange;


                    }
                    db.Prices.Add(price);
                    db.SaveChanges();
                }

                currentPrices = db.Prices.Where(t => t.CurrencyId == CurrencyId).OrderByDescending(t => t.Id).Take(NumberOfPricesContributingToTrend).ToList();
                var curretPrice = currentPrices.FirstOrDefault();
                var previousPrice = currentPrices.Where(t => t.Id < curretPrice.Id).OrderByDescending(t => t.Id).FirstOrDefault();

                if (previousPrice != null)
                {
                  
                        if (curretPrice.CurrencyTickerValue > previousPrice.CurrencyTickerValue)
                        {
                            curretPrice.Trend = Trend.Upwards;
                        }
                        else if (curretPrice.CurrencyTickerValue == previousPrice.CurrencyTickerValue)
                        {
                            curretPrice.Trend = Trend.NoChange;
                        }
                        else
                        {
                            curretPrice.Trend = Trend.Downwards;
                        }
                        db.Entry(curretPrice).State = EntityState.Modified;
                        db.SaveChanges();
                   

                }
                var eariestPrice = db.Prices.Where(t =>t.CurrencyId==CurrencyId && (t.Action.ToUpper() == "BUY" || t.Action.ToUpper() == "SELL" )).OrderByDescending(t => t.Id).FirstOrDefault();
                if (eariestPrice == null)
                {
                    eariestPrice = currentPrices.OrderBy(t => t.CurrencyTickerValue).FirstOrDefault();

                }
                currentPrices = db.Prices.Where(t => t.CurrencyId == CurrencyId && t.Id>=eariestPrice.Id).OrderByDescending(t => t.Id).Take(NumberOfPricesContributingToTrend).ToList();

                curretPrice = currentPrices.OrderByDescending(t => t.Id).FirstOrDefault();
               
                if (curretPrice.PortfolioValue> (curretPrice.BuyValue+ SellAtProfit) && SellAtProfit != 0  && curretPrice.NumberOfTokensInTheSystem>0)
                {



                    UpdateComission("The profit exceeds " + SellAtProfit + " ,it is selling time", CurrencyName);
                   
                     curretPrice.Action = "SELL";
                    curretPrice.CurrencySellValue = curretPrice.PortfolioValue;
                   
                    curretPrice.NumberOfTokensInTheSystem = 0;
                    curretPrice.Logs = "The profit exceeds " + SellAtProfit + " ,it is selling time";
                    db.Entry(curretPrice).State = EntityState.Modified;
                    db.SaveChanges();
                    return;
                }

                if (curretPrice.PortfolioValue < (curretPrice.BuyValue - SellAtLoss) && SellAtLoss != 0 && curretPrice.NumberOfTokensInTheSystem > 0)
                {
                    UpdateComission("The loss exceeds " + SellAtLoss + " ,it is selling time", CurrencyName);
 
                    
                    curretPrice.CurrencySellValue = curretPrice.PortfolioValue;
                  
                      curretPrice.Action = "SELL";
                  curretPrice.NumberOfTokensInTheSystem = 0;
                   curretPrice.Logs = "The loss exceeds " + SellAtLoss + " ,it is selling time";

                    db.Entry(curretPrice).State = EntityState.Modified;
                    db.SaveChanges();

                    return;

                }
                var diff = (curretPrice.CurrencyTickerValue - eariestPrice.CurrencyTickerValue);
                var percentChange = (diff / eariestPrice.CurrencyTickerValue)* 100.0;
                bool isUpWards = false;
                if (percentChange < 0)
                {
                    percentChange = 0 - percentChange;
                    isUpWards = false;
                }
                else
                {
                    isUpWards = true;
                }
                if (percentChange < PercentIncrementForTrend)
                {
                    curretPrice.Logs = "The percent increment("+ percentChange+") for last " + NumberOfPricesContributingToTrend + " is less than " + PercentIncrementForTrend + ",waiting";
                    db.Entry(curretPrice).State = EntityState.Modified;
                    db.SaveChanges();
                    return;
                }
                if (isUpWards && curretPrice.NumberOfTokensInTheSystem == 0)
                {
                
                    if (curretPrice.NumberOfTokensInTheSystem != 0)
                    {
                        curretPrice.Logs = "The first and last values have increased by " + percentChange + " ,it is buying time but all money invested. holding..";
                        curretPrice.Action = "HOLD";
                        db.Entry(curretPrice).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    

                    else
                    {


                        UpdateComission("The first and last values have increased by " + percentChange +", it is buying time" + SellAtLoss + "", CurrencyName);

                        curretPrice.Action = "BUY";
                        curretPrice.NumberOfTokensInTheSystem = (float)(curretPrice.PortfolioValue) / (float)curretPrice.CurrencyTickerValue;
                        curretPrice.CurrencySellValue = 0;
                        curretPrice.BuyValue = curretPrice.PortfolioValue ;
                        curretPrice.Logs = "The first and last values have increased by " + percentChange +",values have been upwards,it is buying time";
                        db.Entry(curretPrice).State = EntityState.Modified;
                        db.SaveChanges();
                        return;

                    }
                }
                if (!isUpWards && curretPrice.NumberOfTokensInTheSystem > 0)
                {

                    if (curretPrice.NumberOfTokensInTheSystem == 0)
                    {
                        curretPrice.Logs = "The first and last values have decreased by " + percentChange +", selling time but everything has been sold already..waiting";
                        curretPrice.Action = "WAITING";
                        db.Entry(curretPrice).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    else
                    {





                        UpdateComission("The first and last values have decreased by " + percentChange +", ,it is selling time", CurrencyName);


                        curretPrice.Action = "SELL";
                        curretPrice.CurrencySellValue = (float)curretPrice.NumberOfTokensInTheSystem.Value * (float)curretPrice.CurrencyTickerValue;
                        curretPrice.NumberOfTokensInTheSystem = 0;
                       
                       curretPrice.Logs = "The first and last values have decreased by " + percentChange + ", it is selling time";
                        db.SaveChanges();
                        return;



                    }
                }

            }
       
           
            return;
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}