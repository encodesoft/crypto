﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using crypto.Models;
using PagedList;

namespace crypto.Controllers
{
    [Authorize]
    public class BankBalancesController : Controller
    {
        public int pageSize = 30;
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: BankBalances
        public ActionResult Index(int? id)
        {
            return View(db.BankBalances.OrderByDescending(t=>t.Id).ToPagedList(id ?? 1, pageSize));
        }

        // GET: BankBalances/Logs/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankBalance bankBalance = db.BankBalances.Find(id);
            if (bankBalance == null)
            {
                return HttpNotFound();
            }
            return View(bankBalance);
        }

        // GET: BankBalances/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BankBalances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Balance,Descripton,DateCreated,DateUpdated,UserCreated,UserModified")] BankBalance bankBalance)
        {
            if (ModelState.IsValid)
            {
                db.BankBalances.Add(bankBalance);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bankBalance);
        }

        // GET: BankBalances/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankBalance bankBalance = db.BankBalances.Find(id);
            if (bankBalance == null)
            {
                return HttpNotFound();
            }
            return View(bankBalance);
        }

        // POST: BankBalances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Balance,Descripton,DateCreated,DateUpdated,UserCreated,UserModified")] BankBalance bankBalance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bankBalance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bankBalance);
        }

        // GET: BankBalances/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankBalance bankBalance = db.BankBalances.Find(id);
            if (bankBalance == null)
            {
                return HttpNotFound();
            }
            return View(bankBalance);
        }

        // POST: BankBalances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BankBalance bankBalance = db.BankBalances.Find(id);
            db.BankBalances.Remove(bankBalance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
