﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using crypto.Models;
using PagedList;

namespace crypto.Controllers
{
    [Authorize]
    public class PricesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public int pageSize = 30;

        // GET: Prices
        public ActionResult Index(int currencyId,int? id)
        {
            ViewBag.currencyId = currencyId;
            var prices = db.Prices.ToList();
            if(prices.Count==0)
            {
                prices = new List<Price>();
            }
           return View(prices.Where(t=>t.CurrencyId==currencyId).OrderByDescending(t => t.Id).ToPagedList(id ?? 1, pageSize));

        }
        public ActionResult Buys(int currencyId, int? id)
        {
            ViewBag.currencyId = currencyId;
            var prices = db.Prices.Where(t => t.Action.ToUpper() == "BUY").ToList();
            if (prices.Count == 0)
            {
                prices = new List<Price>();
            }
            return View(prices.Where(t => t.CurrencyId == currencyId).OrderByDescending(t => t.Id).ToPagedList(id ?? 1, pageSize));

        }
        public ActionResult Sells(int currencyId, int? id)
        {
            ViewBag.currencyId = currencyId;
            var prices = db.Prices.Where(t => t.Action.ToUpper() == "SELL").ToList();
            if (prices.Count == 0)
            {
                prices = new List<Price>();
            }
            return View(prices.Where(t => t.CurrencyId == currencyId).OrderByDescending(t => t.Id).ToPagedList(id ?? 1, pageSize));

        }
        public ActionResult Trades(int currencyId, int? id)
        {
            ViewBag.currencyId = currencyId;
            var prices = db.Prices.Where(t => t.Action.ToUpper() == "SELL"||t.Action.ToUpper()=="BUY").ToList();
            if (prices.Count == 0)
            {
                prices = new List<Price>();
            }
            return View(prices.Where(t => t.CurrencyId == currencyId).OrderByDescending(t => t.Id).ToPagedList(id ?? 1, pageSize));

        }
        // GET: Prices/Logs/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price price = db.Prices.Find(id);
            if (price == null)
            {
                return HttpNotFound();
            }
            return View(price);
        }

        // GET: Prices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Prices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CurrencyName,CurrencyTickerValue,Trend,Logs,NotInvestedValue,CurrentValue,BuyValue,NumberOfTokensInTheSystem,SellValue,ProfitLoss,Action,DateCreated,DateUpdated,UserCreated,UserModified")] Price price)
        {
            if (ModelState.IsValid)
            {
                db.Prices.Add(price);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(price);
        }

        // GET: Prices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price price = db.Prices.Find(id);
            if (price == null)
            {
                return HttpNotFound();
            }
            return View(price);
        }

        // POST: Prices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CurrencyName,CurrencyTickerValue,Trend,Logs,NotInvestedValue,CurrentValue,BuyValue,NumberOfTokensInTheSystem,SellValue,ProfitLoss,Action,DateCreated,DateUpdated,UserCreated,UserModified")] Price price)
        {
            if (ModelState.IsValid)
            {
                db.Entry(price).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(price);
        }

        // GET: Prices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price price = db.Prices.Find(id);
            if (price == null)
            {
                return HttpNotFound();
            }
            return View(price);
        }
        public ActionResult DeleteAll()
        {
           
            var prices = db.Prices.ToList();
            db.Prices.RemoveRange(prices);
            db.SaveChanges();
            return View("Index");
        }
        // POST: Prices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Price price = db.Prices.Find(id);
            db.Prices.Remove(price);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
