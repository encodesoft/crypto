﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using crypto.Models;
using PagedList;

namespace crypto.Controllers
{
    public class ComissionFeesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public int pageSize = 30;

        // GET: ComissionFees
        public ActionResult Index(int? id)
        {
            var com = db.ComissionFees.ToList();
            if (com.Count() > 0)
            {
                ViewBag.TotalFee = db.ComissionFees.Sum(t => t.Fee);

            }
            else
            {
                ViewBag.TotalFee = 0;
            }
            return View(db.ComissionFees.OrderByDescending(t => t.Id).ToPagedList(id ?? 1, pageSize));

        }

        // GET: ComissionFees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComissionFee comissionFee = db.ComissionFees.Find(id);
            if (comissionFee == null)
            {
                return HttpNotFound();
            }
            return View(comissionFee);
        }

        // GET: ComissionFees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ComissionFees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RunningTotal,Fee,Descripton,DateCreated,DateUpdated,UserCreated,UserModified")] ComissionFee comissionFee)
        {
            if (ModelState.IsValid)
            {
                db.ComissionFees.Add(comissionFee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(comissionFee);
        }

        // GET: ComissionFees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComissionFee comissionFee = db.ComissionFees.Find(id);
            if (comissionFee == null)
            {
                return HttpNotFound();
            }
            return View(comissionFee);
        }

        // POST: ComissionFees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RunningTotal,Fee,Descripton,DateCreated,DateUpdated,UserCreated,UserModified")] ComissionFee comissionFee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comissionFee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(comissionFee);
        }

        // GET: ComissionFees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ComissionFee comissionFee = db.ComissionFees.Find(id);
            if (comissionFee == null)
            {
                return HttpNotFound();
            }
            return View(comissionFee);
        }

        // POST: ComissionFees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ComissionFee comissionFee = db.ComissionFees.Find(id);
            db.ComissionFees.Remove(comissionFee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
