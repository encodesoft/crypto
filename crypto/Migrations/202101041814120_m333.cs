namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m333 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankBalances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Descripton = c.String(),
                        DateCreated = c.DateTime(),
                        DateUpdated = c.DateTime(),
                        UserCreated = c.String(),
                        UserModified = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BankBalances");
        }
    }
}
