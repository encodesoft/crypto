namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sssd : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prices", "NumberOfTokensInTheSystem", c => c.Single());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prices", "NumberOfTokensInTheSystem", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
