namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sad : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "CurrencyBuyValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "CurrencySellValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "PortfolioValue", c => c.Single(nullable: false));
            DropColumn("dbo.Prices", "CurrencyBuyTickerValue");
            DropColumn("dbo.Prices", "PortfolioValue");
            DropColumn("dbo.Prices", "NetCurrentProfitOrLossAfterFee");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prices", "NetCurrentProfitOrLossAfterFee", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "PortfolioValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "CurrencyBuyTickerValue", c => c.Single(nullable: false));
            DropColumn("dbo.Prices", "PortfolioValue");
            DropColumn("dbo.Prices", "CurrencySellValue");
            DropColumn("dbo.Prices", "CurrencyBuyValue");
        }
    }
}
