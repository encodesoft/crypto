namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig23 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prices", "TickerPrice", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prices", "TickerPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
