// <auto-generated />
namespace crypto.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class m333 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(m333));
        
        string IMigrationMetadata.Id
        {
            get { return "202101041814120_m333"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
