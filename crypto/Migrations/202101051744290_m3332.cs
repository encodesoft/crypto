namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m3332 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "NetCurrentProfitOrLossAfterFee", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prices", "NetCurrentProfitOrLossAfterFee");
        }
    }
}
