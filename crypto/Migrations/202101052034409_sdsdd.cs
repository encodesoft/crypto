namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sdsdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CurrencyName = c.String(),
                        InitialMoney = c.Int(nullable: false),
                        SellAtProfit = c.Int(nullable: false),
                        SellAtLoss = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        DateUpdated = c.DateTime(),
                        UserCreated = c.String(),
                        UserModified = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Prices", "CurrencyId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prices", "CurrencyId");
            DropTable("dbo.Currencies");
        }
    }
}
