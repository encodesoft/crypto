namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m3331 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "CurrencyName", c => c.String(nullable: false));
            AddColumn("dbo.Prices", "CurrencyTickerValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "CurrencyBuyTickerValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "PortfolioValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "Logs", c => c.String());
            DropColumn("dbo.Prices", "Name");
            DropColumn("dbo.Prices", "Amount");
            DropColumn("dbo.Prices", "Message");
            DropColumn("dbo.Prices", "NotInvestedValue");
            DropColumn("dbo.Prices", "CurrentValue");
            DropColumn("dbo.Prices", "BuyValue");
            DropColumn("dbo.Prices", "SellValue");
            DropColumn("dbo.Prices", "ProfitLoss");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prices", "ProfitLoss", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Prices", "SellValue", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Prices", "BuyValue", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Prices", "CurrentValue", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Prices", "NotInvestedValue", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Prices", "Message", c => c.String());
            AddColumn("dbo.Prices", "Amount", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "Name", c => c.String(nullable: false));
            DropColumn("dbo.Prices", "Logs");
            DropColumn("dbo.Prices", "PortfolioValue");
            DropColumn("dbo.Prices", "CurrencyBuyTickerValue");
            DropColumn("dbo.Prices", "CurrencyTickerValue");
            DropColumn("dbo.Prices", "CurrencyName");
        }
    }
}
