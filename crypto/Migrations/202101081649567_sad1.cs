namespace crypto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sad1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prices", "BuyValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "PortfolioValue", c => c.Single(nullable: false));
            DropColumn("dbo.Prices", "CurrencyBuyValue");
            DropColumn("dbo.Prices", "NetPortfolioValue");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Prices", "NetPortfolioValue", c => c.Single(nullable: false));
            AddColumn("dbo.Prices", "CurrencyBuyValue", c => c.Single(nullable: false));
            DropColumn("dbo.Prices", "PortfolioValue");
            DropColumn("dbo.Prices", "BuyValue");
        }
    }
}
