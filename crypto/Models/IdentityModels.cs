﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace crypto.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public int UserNumber { get; set; }
     
        public DateTime DateCreated { get; set; } = DateTime.Now;

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseRecord && (x.State == EntityState.Added || x.State == EntityState.Modified));

            var currentUsername = !string.IsNullOrEmpty(System.Web.HttpContext.Current?.User?.Identity?.Name)
                ? HttpContext.Current.User.Identity.Name
                : "Anonymous";

            foreach (var entity in entities)
            {

                if (entity.State == EntityState.Added)
                {
                    ((BaseRecord)entity.Entity).DateCreated = DateTime.UtcNow;
                    ((BaseRecord)entity.Entity).UserCreated = currentUsername;
                }

                ((BaseRecord)entity.Entity).DateUpdated = DateTime.UtcNow;
                ((BaseRecord)entity.Entity).UserModified = currentUsername;
            }
        }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<crypto.Models.Price> Prices { get; set; }
        public System.Data.Entity.DbSet<crypto.Models.BankBalance> BankBalances { get; set; }

        public System.Data.Entity.DbSet<crypto.Models.Currency> Currencies { get; set; }
        public System.Data.Entity.DbSet<crypto.Models.ComissionFee> ComissionFees { get; set; }
    }
}