﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Web.Security;
using System.Web;

namespace crypto.Models
{
   public class BaseRecord
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? DateCreated { get; set; }

        [ScaffoldColumn(false)]
        public DateTime? DateUpdated { get; set; }

        [ScaffoldColumn(false)]
        public string UserCreated { get; set; }

        [ScaffoldColumn(false)]
        public string UserModified { get; set; }


        [ScaffoldColumn(false)]
        [NotMapped]
        public string MethodName {
            get
            {
                  if(Id==0)
                {
                    return "Create";
                }
                  else
                {
                    return "Edit";
                }
            }
            private set { }
            
         }
    }
}



