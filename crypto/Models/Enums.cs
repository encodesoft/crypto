﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

  
namespace crypto.Models
{
     public enum Trend
    {
        [Display(Name = "Select")]
        Select,
        [Display(Name = "Upwards")]
        Upwards,
        [Display(Name = "Downwards")]
        Downwards,        
       [Display(Name = "NoChange")]
        NoChange


    }

}