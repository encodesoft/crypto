﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;

namespace crypto.Models
{
    public class Price : BaseRecord
    {
       [Required]
        public int CurrencyId { get; set; }
       [Required]
        [Display(Name = "CurrencyName")]
        public string CurrencyName { get; set; }

        public float CurrencyTickerValue { get; set; }
        public float BuyValue { get; set; }

        public float CurrencySellValue { get; set; }

        public float PortfolioValue { get; set; }
        
       [NotMapped]
        public float CurrentProfitLoss
        {
            get
            {
                if(NumberOfTokensInTheSystem!=0)
                {
                    return  PortfolioValue- BuyValue;
                }
                else

                {
                    return 0;
                }

            }
        }



        public float? NumberOfTokensInTheSystem { get; set; }


        public Trend Trend { get; set; }

        public string Logs { get; set; }

       
        public string Action { get; set; }

    }
}