﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;

namespace crypto.Models
{
    public class Currency : BaseRecord
    {
        public string CurrencyName { get; set; }
        public int  InitialMoney { get; set; }
        public int SellAtProfit { get; set; }
        public int SellAtLoss { get; set; }


    }
}