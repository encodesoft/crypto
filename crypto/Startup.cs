﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(crypto.Startup))]
namespace crypto
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
